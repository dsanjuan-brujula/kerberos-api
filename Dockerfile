FROM tomcat

ENV CATALINA_OPTS='-Dlogs.path=/usr/local/tomcat/logs'

RUN ["rm", "-rf", "/usr/local/tomcat/webapps/*"]
ADD ./build/libs/kerberos-api*.war /usr/local/tomcat/webapps/kerberos-api.war

EXPOSE 8080
CMD ["catalina.sh", "run"]