pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew build -x test -x integTest'
            }
        }
        stage("SonarQube analysis") {
            steps {
                withSonarQubeEnv('SonarQube') {
                    sh './gradlew -Dsonar.host.url=$SONAR_HOST_URL -Dsonar.login=$SONAR_AUTH_TOKEN --info sonarqube'
                }
            }
        }
        stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    // Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
                    // true = set pipeline to UNSTABLE, false = don't
                    // Requires SonarQube Scanner for Jenkins 2.7+
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Run Unit Test') {
            when {
                expression {
                    return env.BRANCH_NAME != 'master';
                }
            }
            steps {
                echo 'Unit Testing..'
                sh './gradlew test -x integTest'
            }
        }

        stage('Deploy') {
            options {
                timeout(time: 10, unit: 'MINUTES')
            }
            steps {
                script {
                    echo 'Deploying....'
                    if ( currentBuild.result != 'SUCCESS') {
                        input  message: "Press Ok to continue", submitter: "dsanjuan"
                    }
                    sh 'printenv'
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
        failure {
            echo 'I will send email'
            emailext body: "${currentBuild.currentResult}: Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}",
                                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']],
                                subject: "Jenkins Build ${currentBuild.currentResult}: Job ${env.JOB_NAME}"
        }
    }
}