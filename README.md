#Kerberos API

##Introducción
API rest para gestion usuarios.
 
##Aplicación

Para gestión de dependencias y tareas se emplea Gradle. Para la publicación del API se emplea Jersey con Spring Boot. 
Se emplea una implementación log4j2 para generar los logs.

###Configuracion

Se pueden localizar las propiedades de la aplicación en el directorio _resources_. Existe un fichero común a todos los
entornos y un fichero por entorno con las propiedades especificas del mismo.

Las propiedades para los mensajes internacionalizados se encuentran alojadas en _resources/i18n/messages\_[lang].properties_

###Construcción

La construcción y gestión de dependencias se realiza mediante Gradle. Se emplea un wrapper para que no sea necesario
instalar gradle en el equipo.

Para construir el fichero _.war_, para por ejemplo, desplegar en un tomcat no integrado en el IDE ejecutamos el wrapper de gradle:
```bash
gradlew clean bootWar
```
Para obtener un listado de las tareas que podemos lanzar:
```bash
gradlew tasks
```

En caso de emplear eclipse o intellij se han incluido los plugin de gradle 'idea' y 'eclipse'.

#### Database

Es necesario disponer de una instancia de MySQL. Para el control de cambios en la base de datos se han incluido scripts liquibase (/src/main/db). 

Una vez se disponga de una instancia de Base de datos
MySQL, es posible aplicar los scripts necesarios mediante el plugin de liquibase configurado para gradle. En primer lugar es necesario configurar una
serie de properites en el fichero gradle.properties de vuestro usuario (~/.gradle/gradle.properties). Las propiedades necesarias a configurar serían:
 
 * _mainUrl_: URL que usará el driver para conectar (ej: jdbc:mysql://localhost/kerberos)
 * _mainUsername_: Usuario para conectarse a la base de datos
 * _mainPassword_: Contraseña del usuario con el que se conectará a base de datos el plugin de liquibase.
 
Una vez configuradas las properties adecuadamente, es posible aplicar los cambios pendientes a la base de datos mediante el comando:

```bash
gradlew update
```

#### Sonar

Para la generación del reporte sonar es necesario definir una propiedad con el token de acceso en el fichero _gradle.properties_:
* **systemProp.sonar.login**: token usuario de acceso a sonar.

Para generar el reporte:

```batch
gradlew -Dsonar.branch=[branch] --info sonarqube
```

donde:
* _branch_: rama en la que estamos situados cuando generamos el reporte. Esto generará un proyecto en sonar diferente por
rama.

###Despliegue

####Configuración entorno

Es necesario configurar una serie de propiedades de entorno. Las propiedades necesarias son:

* **logs.path**: path donde queremos alojar los ficheros de logs generados por la aplicación.
* **spring.profiles.active**: perfil de spring que deseamos activar. Posibles valores:
    * _local_: perfil para una ejecución en nuestros entornos locales.
    * _pro_: perfil para el entorno de producción.
* **spring.datasource.username**: usuario de base de datos que empleará la aplicación.
* **spring.datasource.password**: password del usuario de base de datos que empleará la aplicación.
* **rest.token.secret-key**: secret que se empleará para encriptar los tokens.

####Local

#####Spring boot gradle plugin

Actualmente se está generando un fichero .war ejecutable. Es posible ejecutar la aplicación empleando el plugin de gradle
empleando el siguiente comando:

```bash
gradlew bootRun -Dlogs.path=[path-for-logs] -Dspring.profiles.active=local -Dspring.datasource.username=[user] -Dspring.datasource.password=[pass]
```

donde:
* _path-for-logs_: ruta donde se generarán los ficheros de logs.
* _user_: usuario de base de datos de la aplicación.
* _pass_: password de base de datos de la aplicación.

##### IDE (Tomcat Server Local)

Puedes desplegar el fichero .war en un tomcat local.

Para definir las variables en Tomcat:

```bash
-Dlogs.path=<path> -Dspring.profiles.active=<environment-profile> -Dspring.datasource.username=<user> -Dspring.datasource.password=<pass>
```

#####Docker

En primer lugar es necesario construir la imagen para poder desplegar nuestra aplicación en un contenedor de docker.
Para ello, ubicado en el directorio raiz, ejecuta:

```bash
docker build -t kerberos-api .
```
Este comando generará una imagen empleando el fichero _Dockerfile_ existente en la raiz del proyecto.
Para comprobar que se ha generado la imagen correctamente puedes ejecutar:

```bash
 docker images
```

Una vez generada la imagen puedes ejecutarla en un contenedor mediante el comando:

```bash
 docker run -e "spring.datasource.username={datasource-user}" -e "spring.datasource.password={datasource-pass}" -p 8080:8080 -t kerberos-api
```
Para comprobar que efectivamente nuestra aplicación está levantada en el contenedor docker, accede a
la url:
```http request
http://localhost:8080/kerberos-api/[current-version]/actuator/health
```

Para debug:

```bash
 docker run -e "JAVA_OPTS=-agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=n" -p 8080:8080 -p 5005:5005 -t kerberos-api
```

Más información sobre el comando run [aquí](https://docs.docker.com/engine/reference/run/).

### Monitorización

La aplicación cuenta con una serie de endpoints que permiten obtener información sobre el estado actual de la misma. Se 
encuentran activos todos los endpoints que ofrece [Spring Actuator](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html)

Los más destacados:
* _/actuator/health_: muestra información de la salud de la aplicación (database, etc)
* _/actuator/metrics_: permite obtener diferente métricas de la aplicación. Al consultar este endpoint obtenemos
una lista de posibles métricas a consultar. Podemos consultar cada una de ellas ralizando una llamada a:

```http request
GET [base-path]/v1/actuator/metrics/[metric-id]
```

donde _metric_id_ sería por ejemplo **system.cpu.usage**.

