CREATE USER 'kerberos'@'%';
ALTER USER 'kerberos'@'%'
IDENTIFIED BY 'qwerty12345' ;
GRANT Alter ON kerberos.* TO 'kerberos'@'%';
GRANT Create ON kerberos.* TO 'kerberos'@'%';
GRANT Create view ON kerberos.* TO 'kerberos'@'%';
GRANT Delete ON kerberos.* TO 'kerberos'@'%';
GRANT Drop ON kerberos.* TO 'kerberos'@'%';
GRANT Index ON kerberos.* TO 'kerberos'@'%';
GRANT Insert ON kerberos.* TO 'kerberos'@'%';
GRANT References ON kerberos.* TO 'kerberos'@'%';
GRANT Select ON kerberos.* TO 'kerberos'@'%';
GRANT Show view ON kerberos.* TO 'kerberos'@'%';
GRANT Trigger ON kerberos.* TO 'kerberos'@'%';
GRANT Update ON kerberos.* TO 'kerberos'@'%';
FLUSH PRIVILEGES;

CREATE TABLE kerberos.USERS (
	USER_NAME varchar(20) NOT NULL,
	FULL_NAME varchar(200) NOT NULL,
	PASSWORD varchar(100) NOT NULL,
	UPDATED_AT TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT NewTable_PK PRIMARY KEY (USER_NAME)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_0900_ai_ci;

