package com.brujula.kerberos;

public final class Formats {
    public static final String FULL_NAME_PATTERN = "^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ]+$";
    public static final String USER_NAME_PATTERN = "^[a-z0-9]+$";
    public static final String PASS_PATTERN = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$";

    public static final int FULL_NAME_MIN_SIZE = 3;
    public static final int FULL_NAME_MAX_SIZE = 200;

    public static final int USER_NAME_MIN_SIZE = 3;
    public static final int USER_NAME_MAX_SIZE = 20;

    public static final int PASSWORD_MIN_SIZE = 8;

    private Formats(){}

}
