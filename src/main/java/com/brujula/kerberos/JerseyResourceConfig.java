package com.brujula.kerberos;

import com.brujula.kerberos.rest.dto.PageToken;
import com.brujula.kerberos.rest.encoder.Encoder;
import com.brujula.kerberos.rest.filters.CorsFilter;
import com.brujula.kerberos.rest.serialization.PageTokenSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;
import java.text.SimpleDateFormat;

@Configuration
@ApplicationPath("/v1")
public class JerseyResourceConfig extends ResourceConfig {

	public JerseyResourceConfig() {
		packages("com.brujula.kerberos.rest");
	}

	@Bean
	public ObjectMapper mapper(JsonSerializer<PageToken> pageTokenJsonSerializer) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

		SimpleModule module = new SimpleModule();
		module.addSerializer(PageToken.class, pageTokenJsonSerializer);
		mapper.registerModule(module);
		mapper.registerModule(new JavaTimeModule());
		mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		mapper.configure( DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		mapper.setDateFormat(new SimpleDateFormat(DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.getPattern()));
		return mapper;
	}

	@Bean
	public JsonSerializer<PageToken> ratePlanCodeSerializer(Encoder encoder) {
		return new PageTokenSerializer(encoder);
	}
}
