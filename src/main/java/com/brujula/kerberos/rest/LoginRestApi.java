package com.brujula.kerberos.rest;


import com.brujula.kerberos.rest.dto.AuthToken;
import com.brujula.kerberos.rest.dto.LoginDto;
import com.brujula.kerberos.rest.dto.TokenInfo;
import com.brujula.kerberos.rest.encoder.TokenHandler;
import com.brujula.kerberos.rest.exception.UnauthorizedException;
import com.brujula.kerberos.service.ILoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/tokens")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public class LoginRestApi {

    private ILoginService service;

    private TokenHandler<TokenInfo> tokenHandler;

    @Autowired
    public LoginRestApi(final ILoginService loginService, TokenHandler tokenHandler) {
        this.service = loginService;
        this.tokenHandler = tokenHandler;
    }

    @POST
    public AuthToken doLogin(final @Valid LoginDto loginDto) {
        if (service.login(loginDto.getUserName(), loginDto.getPassword())) {
            return tokenHandler.encode(new TokenInfo(loginDto.getUserName()));
        } else {
            throw new UnauthorizedException();
        }
    }

    @GET
    @Path("/{token}")
    public TokenInfo getTokenInfo(final @PathParam("token") String tokenValue) {
        return tokenHandler.decode(tokenValue);

    }
}
