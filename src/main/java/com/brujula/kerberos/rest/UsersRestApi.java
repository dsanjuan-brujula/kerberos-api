package com.brujula.kerberos.rest;

import com.brujula.kerberos.rest.adapter.PaginationParamsAdapter;
import com.brujula.kerberos.rest.dto.*;
import com.brujula.kerberos.service.IUserService;
import com.brujula.kerberos.service.entity.IPage;
import com.brujula.kerberos.service.entity.Page;
import com.brujula.kerberos.service.jpa.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public class UsersRestApi {

    private IUserService userService;

    private PaginationParamsAdapter paramsAdapter;

    private Mapper<com.brujula.kerberos.service.entity.User, User> mapper;


    @Autowired
    public UsersRestApi(final IUserService userService,
                        PaginationParamsAdapter paramsAdapter,
                        Mapper<com.brujula.kerberos.service.entity.User, User> mapper) {
        this.userService = userService;
        this.paramsAdapter = paramsAdapter;
        this.mapper = mapper;
    }

    @GET
    public CollectionResponse<List<User>> getUsers(@QueryParam("pageSize") Integer pageSize,
                                                   @QueryParam("pageToken") String pageToken){
        IPage page = paramsAdapter.handlePage(pageSize, pageToken);
        List<com.brujula.kerberos.service.entity.User> users;
        Pagination pagination = null;

        if (page != null) {
            Page<com.brujula.kerberos.service.entity.User> result = userService.getByPage(page);
            int nextPage = page.getNumber() + 1;
            pagination = new Pagination(result.getTotalElements(), result.getTotalPages(),
                    nextPage < result.getTotalPages() ? new PageToken(nextPage, result.getPageSize()) : null);
            users = result.getContent();
        } else {
            users = userService.getAll();
        }

        return CollectionResponse.builder(users.stream().map(user -> mapper.from(user)).collect(Collectors.toList()))
                .withPagination(pagination)
                .build();
    }

    @POST
    public void createUser(final @Valid User userRq) {
        userService.create(mapper.to(userRq));
    }

    @GET
    @Path("/{userName}")
    public User getUser(@PathParam("userName") String userName) {
        return mapper.from(userService.getByUserName(userName).orElseThrow(NotFoundException::new));
    }

    @PUT
    public void updateOrCreateUser(final @Valid User userRq) {
        userService.updateOrCreate(mapper.to(userRq));

    }

    @PATCH
    @Path("/{userName}")
    public User updateUser(final @PathParam("userName") String userName, final @Valid UpdateUserRq userRq) {
        return mapper.from( userService.update(userName, userRq.getPassword(), userRq.getFullName()));
    }


}
