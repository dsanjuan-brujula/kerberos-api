package com.brujula.kerberos.rest.adapter;

import com.brujula.kerberos.rest.dto.PageToken;
import com.brujula.kerberos.rest.encoder.Encoder;
import com.brujula.kerberos.service.entity.IPage;
import com.brujula.kerberos.service.entity.Page;
import org.springframework.stereotype.Service;

@Service
public class PaginationParamsAdapter {

    private Encoder encoder;

    public PaginationParamsAdapter(Encoder encoder) {
        this.encoder = encoder;
    }

    public IPage handlePage(final Integer pageSize, String pageToken) {
        Integer finalPageSize = pageSize;
        Integer finalPage = null;
        IPage page = null;
        if (pageToken != null) {
            PageToken token = encoder.decode(pageToken, PageToken.class);
            finalPageSize = token.getPageSize();
            finalPage = token.getPage();
        }
        if (finalPageSize != null) {
            Page.PageReqBuilder builder = Page.pageReq(finalPageSize);
            if (finalPage != null) {
                builder.withPage(finalPage);
            }
            page = builder.build();
        }
        return page;
    }
}
