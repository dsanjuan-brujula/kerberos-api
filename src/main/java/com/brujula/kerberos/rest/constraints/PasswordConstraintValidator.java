package com.brujula.kerberos.rest.constraints;

import com.brujula.kerberos.Formats;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordConstraintValidator implements ConstraintValidator<Password, String>
{

	@Override
	public boolean isValid( String value, ConstraintValidatorContext context )
	{
		boolean result = true;
		if (value != null)
		{
			final Pattern pattern = Pattern.compile( Formats.PASS_PATTERN, Pattern.MULTILINE );
			final Matcher matcher = pattern.matcher( value );
			result = matcher.find();
		}
		return result;
	}
}
