package com.brujula.kerberos.rest.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AuthToken {

    @NotNull
    @NotEmpty
    private String token;

    public AuthToken(@NotNull @NotEmpty String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
