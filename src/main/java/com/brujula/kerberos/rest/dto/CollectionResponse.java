package com.brujula.kerberos.rest.dto;

public class CollectionResponse<T> {

    private Pagination pagination;
    private T data;

    private CollectionResponse(final Builder<T> builder) {
        this.data = builder.data;
        this.pagination = builder.pagination;
    }

    public static <K> Builder<K> builder(K data) {
        return new Builder<>(data);
    }

    public Pagination getPagination() {
        return pagination;
    }

    public T getData() {
        return data;
    }

    public static class Builder<G> {
        private Pagination pagination;
        private G data;

        public Builder(final G data) {
            this.data = data;
        }

        public Builder<G> withPagination(Pagination pagination) {
            this.pagination = pagination;
            return this;
        }

        public CollectionResponse<G> build() {
            return new CollectionResponse<>(this);
        }
    }
}
