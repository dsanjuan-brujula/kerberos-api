package com.brujula.kerberos.rest.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class ErrorDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String messageCode;
	private transient List<Object> parameters;
	
	public ErrorDetail(final Builder builder) {
		this.messageCode = builder.messageCode;
		this.parameters = builder.parameters;
	}

	public String getMessageCode() {
		return messageCode;
	}

	public List<Object> getParameters() {
		return parameters;
	}

	public static class Builder {
		private String messageCode;
		private List<Object> parameters;
		
		public Builder(final String messageCode) {
			this.messageCode = messageCode;
		}

		public Builder withParameters(final Object ... parameters) {
			this.parameters = Arrays.asList(parameters);
			return this;
		}
		
		public ErrorDetail build() {
			return new ErrorDetail(this);
		}
	}
}
