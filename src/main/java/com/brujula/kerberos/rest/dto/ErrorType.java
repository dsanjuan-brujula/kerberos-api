package com.brujula.kerberos.rest.dto;

public enum ErrorType {
	FATAL, WARNING
}
