package com.brujula.kerberos.rest.dto;

public class PageToken {

    private int page;
    private int pageSize;

    public PageToken() {
    }

    public PageToken(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
