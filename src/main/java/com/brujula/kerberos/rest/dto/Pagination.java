package com.brujula.kerberos.rest.dto;

public class Pagination {

    private long totalElements;
    private int totalPages;
    private PageToken nextPageToken;

    public Pagination(long totalElements, int totalPages, PageToken nextPageToken) {
        this.totalElements = totalElements;
        this.totalPages = totalPages;
        this.nextPageToken = nextPageToken;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public PageToken getNextPageToken() {
        return nextPageToken;
    }

}
