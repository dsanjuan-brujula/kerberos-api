package com.brujula.kerberos.rest.dto;

public class TokenInfo {

    private String userName;

    public TokenInfo(){}

    public TokenInfo(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
