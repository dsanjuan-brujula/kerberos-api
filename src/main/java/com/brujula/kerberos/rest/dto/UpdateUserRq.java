package com.brujula.kerberos.rest.dto;

import com.brujula.kerberos.Formats;
import com.brujula.kerberos.rest.constraints.Password;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class UpdateUserRq implements Serializable {

    @Size(min = Formats.FULL_NAME_MIN_SIZE, max = Formats.FULL_NAME_MAX_SIZE)
    @Pattern(regexp = Formats.FULL_NAME_PATTERN)
    private String fullName;

    @Size(min = Formats.PASSWORD_MIN_SIZE)
    @Password
    private String password;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
