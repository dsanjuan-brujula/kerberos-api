package com.brujula.kerberos.rest.dto;

import com.brujula.kerberos.Formats;
import com.brujula.kerberos.rest.constraints.Password;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

    @NotNull
    @NotEmpty
    @Size(min = Formats.FULL_NAME_MIN_SIZE, max = Formats.FULL_NAME_MAX_SIZE )
    @Pattern(regexp = Formats.FULL_NAME_PATTERN)
    private String fullName;

    @NotNull
    @NotEmpty
    @Size(min = Formats.USER_NAME_MIN_SIZE, max = Formats.USER_NAME_MAX_SIZE)
    @Pattern(regexp = Formats.USER_NAME_PATTERN)
    private String userName;

    @NotNull
    @NotEmpty
    @Size(min = Formats.PASSWORD_MIN_SIZE)
    @Password(message = "validation.password.invalid")
    private String password;

    private Date lastUpdate;

    public String getFullName() {
        return fullName;
    }

    public String getUserName() {
        return userName;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setFullName( String fullName) {
        this.fullName = fullName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
