package com.brujula.kerberos.rest.encoder;

public interface Encoder {

    <T> T decode(String data, Class<T> toClass);
    
    String encode(Object data);

}
