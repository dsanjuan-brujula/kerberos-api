package com.brujula.kerberos.rest.encoder;

import com.brujula.kerberos.rest.dto.AuthToken;

public interface TokenHandler<T> {

    AuthToken encode(final T data);

    T decode(final String value);
}
