package com.brujula.kerberos.rest.encoder.base64;

import com.brujula.kerberos.rest.encoder.Encoder;
import com.brujula.kerberos.rest.exception.ParamsAdapterException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Base64;

@Component
public class JsonBase64Encoder implements Encoder {

    private ObjectMapper mapper;

    public JsonBase64Encoder() {
        this.mapper = new ObjectMapper();
        this.mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @Override
    public <T> T decode(final String base64, final Class<T> toClass) {
        try {
            return mapper.readValue(new String(Base64.getDecoder().decode(base64)), toClass);
        } catch (IOException exc) {
            throw new ParamsAdapterException(exc);
        }
    }

    @Override
    public String encode(final Object data) {
        try {
            return Base64.getEncoder().encodeToString(mapper.writeValueAsString(data).getBytes());
        } catch (final JsonProcessingException exc) {
            throw new ParamsAdapterException(exc);
        }
    }

}
