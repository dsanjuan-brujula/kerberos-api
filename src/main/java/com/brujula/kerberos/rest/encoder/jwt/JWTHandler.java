package com.brujula.kerberos.rest.encoder.jwt;

import com.brujula.kerberos.properties.TokenProperties;
import com.brujula.kerberos.rest.dto.AuthToken;
import com.brujula.kerberos.rest.dto.TokenInfo;
import com.brujula.kerberos.rest.encoder.TokenHandler;
import com.brujula.kerberos.rest.encoder.exception.EncoderException;
import com.brujula.kerberos.rest.exception.UnauthorizedException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.UUID;

@Component
public class JWTHandler implements TokenHandler<TokenInfo> {

	private static final String DATA_KEY = "data";
	
	private ObjectMapper mapper;

	private TokenProperties prop;

	@Autowired
	public JWTHandler(final TokenProperties prop) {
		this.prop = prop;
        this.mapper = new ObjectMapper();
        this.mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }
	
    @Override
    public TokenInfo decode(final String token) {
		try {
    		Jws<Claims> claims = Jwts.parser()
    				.setSigningKey(prop.getSecretKey())
    				.parseClaimsJws(token);
			return mapper.readValue(claims.getBody().get(DATA_KEY).toString(), TokenInfo.class);
		} catch (IOException e) {
			throw new UnauthorizedException(e);
		}
	}

	@Override
    public AuthToken encode(TokenInfo data) {
    	try {
            LocalDateTime now = LocalDateTime.now();
            return new AuthToken(Jwts.builder()
    				.setExpiration( Date.from(now.plusMinutes(prop.getExpiration()).atZone(ZoneId.systemDefault()).toInstant()))
                    .setIssuedAt(Date.from(now.atZone(ZoneId.systemDefault()).toInstant()))
                    .setId(String.valueOf(UUID.randomUUID()))
    				.claim(DATA_KEY, new String(mapper.writeValueAsString(data).getBytes()))
    				.signWith(SignatureAlgorithm.HS512, prop.getSecretKey())
    				.compact());
    	} catch (final JsonProcessingException exc) {
    		throw new EncoderException(exc);
    	}
    }
}
