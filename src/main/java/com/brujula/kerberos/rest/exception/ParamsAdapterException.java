package com.brujula.kerberos.rest.exception;

public class ParamsAdapterException extends RuntimeException {

    public ParamsAdapterException(Throwable cause) {
        super(cause);
    }
}
