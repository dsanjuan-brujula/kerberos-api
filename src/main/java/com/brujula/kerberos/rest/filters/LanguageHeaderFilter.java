package com.brujula.kerberos.rest.filters;

import org.springframework.context.i18n.LocaleContextHolder;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.util.Locale;

@Provider
@Priority(1)
public class LanguageHeaderFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) {
        String languageHeader = requestContext.getHeaderString(HttpHeaders.ACCEPT_LANGUAGE);
        Locale locale = Locale.getDefault();
        if (languageHeader != null && !languageHeader.isEmpty()) {
            try {
                locale = Locale.forLanguageTag(Locale.LanguageRange.parse(languageHeader).get(0).getRange());
            } catch (IllegalArgumentException ex) {
                locale = Locale.getDefault();
            }
        }
        LocaleContextHolder.setLocale(locale);
    }
}
