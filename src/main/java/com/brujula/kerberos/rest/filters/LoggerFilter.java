package com.brujula.kerberos.rest.filters;

import com.brujula.kerberos.rest.logger.AuditLogger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(3)
public class LoggerFilter implements ContainerRequestFilter, ContainerResponseFilter {

    @Autowired
    private AuditLogger<ContainerResponseContext> responseLogger;
    
    @Autowired
    private AuditLogger<ContainerRequestContext> requestLogger;

    @Override
    public void filter(ContainerRequestContext requestContext) {
        requestLogger.log(requestContext);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        responseLogger.log(responseContext);
    }
    

}
