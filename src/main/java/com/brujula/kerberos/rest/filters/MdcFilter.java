package com.brujula.kerberos.rest.filters;

import com.brujula.kerberos.rest.providers.UuIdFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
@Priority(2)
public class MdcFilter implements ContainerRequestFilter, ContainerResponseFilter {

    @Autowired
    private UuIdFactory uuIdFactory;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        MDC.put("request.id", handleRequestId(requestContext));
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        MDC.clear();
    }

    private String handleRequestId(ContainerRequestContext requestContext) {
        String requestId = requestContext.getHeaderString("X-Request-Id");
        if (requestId == null || requestId.isEmpty()) {
            requestId = uuIdFactory.getUuid();
        }
        return requestId;
    }
}
