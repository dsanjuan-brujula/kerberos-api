package com.brujula.kerberos.rest.i18n;

public final class I18nCodes {

    public static final String INVALID_HEADER = "validation.headers";
    public static final String USER_EXISTS = "validation.users.exist";
    public static final String USER_NOT_FOUND = "validation.users.notfound";

    private I18nCodes(){}
}
