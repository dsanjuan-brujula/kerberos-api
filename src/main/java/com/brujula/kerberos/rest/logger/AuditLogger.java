package com.brujula.kerberos.rest.logger;

public interface AuditLogger<T> {
    
    public void log(final T data);
}
