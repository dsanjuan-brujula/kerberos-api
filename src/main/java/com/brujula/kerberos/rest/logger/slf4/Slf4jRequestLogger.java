package com.brujula.kerberos.rest.logger.slf4;

import com.brujula.kerberos.rest.logger.AuditLogger;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;

@Component
public class Slf4jRequestLogger implements AuditLogger<ContainerRequestContext> {

    private Logger logger = LoggerFactory.getLogger(Slf4jRequestLogger.class);

    @Override
    public void log(final ContainerRequestContext requestContext) {
        logger.info("Method: {}", requestContext.getMethod());
        UriInfo uriInfo = requestContext.getUriInfo();
        logger.info("Path: {}", uriInfo.getPath());
        logQueryParameters(uriInfo);
        logHeaders(requestContext);
        logRequestBody(requestContext);

    }

    private void logRequestBody(final ContainerRequestContext requestContext) {
        String body = parseBody(requestContext);
        if (body != null) {
            logger.info("request-body: {}", body);
        }
    }

    private void logHeaders(final ContainerRequestContext requestContext) {
        if (requestContext.getHeaders() != null) {
            String message = parseMultivaluedMap(requestContext.getHeaders());
            if (message != null) {
                logger.info("Headers: {}", message);
            }
        }
    }

    private void logQueryParameters(UriInfo uriInfo) {
        MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
        if (queryParameters != null) {
            String values = parseMultivaluedMap(queryParameters);
            if (values != null) {
                logger.info("QueryParams: {}", values);
            }
        }
    }

    private String parseMultivaluedMap(final MultivaluedMap<String, String> values) {
        return values.keySet().stream().reduce(Strings.EMPTY, (x, key) -> x + ";" + key + ":" + values.get(key));
    }

    private String parseBody(final ContainerRequestContext requestContext) {
        String body = Strings.EMPTY;
        InputStream entityStream = requestContext.getEntityStream();
        if (entityStream != null) {
            StringWriter bodyWriter = new StringWriter();
            try {
                IOUtils.copy(entityStream, bodyWriter, Charset.defaultCharset());
                body = bodyWriter.toString();
                requestContext.setEntityStream(IOUtils.toInputStream(body, Charset.defaultCharset()));
            } catch (final Exception exc) {
                logger.error(ExceptionUtils.getStackTrace(exc));
            }
        }
        return body;
    }
}
