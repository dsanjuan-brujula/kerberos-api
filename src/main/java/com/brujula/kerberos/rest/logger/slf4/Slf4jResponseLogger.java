package com.brujula.kerberos.rest.logger.slf4;

import com.brujula.kerberos.rest.logger.AuditLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.container.ContainerResponseContext;

@Component
public class Slf4jResponseLogger implements AuditLogger<ContainerResponseContext> {

    private Logger logger = LoggerFactory.getLogger(Slf4jResponseLogger.class);

    @Autowired
    private ObjectMapper mapper;

    public Slf4jResponseLogger(final ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public void log(final ContainerResponseContext responseContext) {
        try {
            Object entity = responseContext.getEntity();
            if (entity != null) {
                logBody(entity);
            }
        } catch (final Exception exc) {
            logger.error(ExceptionUtils.getStackTrace(exc));
        }
    }

    private void logBody(Object entity) throws JsonProcessingException {
        String message = mapper.writeValueAsString(entity);
        if (message != null) {
            logger.info("response-body: {}", message);
        }
    }

}
