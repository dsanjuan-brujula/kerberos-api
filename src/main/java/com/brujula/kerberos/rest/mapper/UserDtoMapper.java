package com.brujula.kerberos.rest.mapper;

import com.brujula.kerberos.rest.dto.User;
import com.brujula.kerberos.service.jpa.mapper.Mapper;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.Date;

@Component
public class UserDtoMapper implements Mapper<com.brujula.kerberos.service.entity.User, User> {

    @Override
    public User from(final com.brujula.kerberos.service.entity.User from) {
        User result = new User();
        result.setFullName(from.getFullName());
        handleUserEntityLastUpdate(from, result);
        result.setUserName(from.getUserName());
        result.setPassword(from.getPassword());
        return result;
    }

    @Override
    public com.brujula.kerberos.service.entity.User to(final User to) {
        com.brujula.kerberos.service.entity.User result = new com.brujula.kerberos.service.entity.User(to.getFullName(), to.getUserName(), to.getPassword());
        handleUserLastUpdate(to, result);
        return result;
    }


    private void handleUserEntityLastUpdate(com.brujula.kerberos.service.entity.User from, User result) {
        if (from.getLastUpdate() != null) {
            result.setLastUpdate(Date.from(from.getLastUpdate().atZone(ZoneId.systemDefault()).toInstant()));
        }
    }

    private void handleUserLastUpdate(User to, com.brujula.kerberos.service.entity.User result) {
        if (to.getLastUpdate() != null) {
            result.setLastUpdate(to.getLastUpdate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        }
    }
}
