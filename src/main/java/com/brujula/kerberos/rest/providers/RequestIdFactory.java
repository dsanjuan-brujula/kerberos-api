package com.brujula.kerberos.rest.providers;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class RequestIdFactory implements UuIdFactory {

    @Override
    public String getUuid() {
    	return UUID.randomUUID().toString();        
    }
}
