package com.brujula.kerberos.rest.providers;

public interface UuIdFactory {

    String getUuid();

}
