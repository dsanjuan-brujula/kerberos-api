package com.brujula.kerberos.rest.providers.exception;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ClientExceptionMapper implements ExceptionMapper<ClientErrorException> {

    private Logger logger = LoggerFactory.getLogger(ClientExceptionMapper.class);

    @Override
    public Response toResponse(final ClientErrorException exception) {
        logger.error(ExceptionUtils.getStackTrace(exception));
        return Response.status(exception.getResponse().getStatus())
                .entity(StringUtils.EMPTY).type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
