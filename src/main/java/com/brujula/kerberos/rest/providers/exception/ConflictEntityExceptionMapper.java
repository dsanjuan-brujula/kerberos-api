package com.brujula.kerberos.rest.providers.exception;

import com.brujula.kerberos.rest.dto.ErrorDetail;
import com.brujula.kerberos.rest.dto.ErrorMessage;
import com.brujula.kerberos.service.exception.ConflictEntityException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Arrays;
import java.util.Locale;

@Provider
public class ConflictEntityExceptionMapper implements ExceptionMapper<ConflictEntityException> {

    private Logger logger = LoggerFactory.getLogger(ConflictEntityExceptionMapper.class);

    private MessageSource messageSource;

    @Autowired
    public ConflictEntityExceptionMapper(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public Response toResponse(final ConflictEntityException exception) {
        logger.error(ExceptionUtils.getStackTrace(exception));
        return Response.status(Response.Status.CONFLICT)
                .entity(Arrays.asList(handleErrorDetail( exception.getDetail(), LocaleContextHolder.getLocale())))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

    private ErrorMessage handleErrorDetail(final ErrorDetail errorDetail, final Locale locale) {
        Object[] args = null;
        if (errorDetail.getParameters() != null) {
            args = errorDetail.getParameters().toArray();
        }
        return new ErrorMessage(messageSource.getMessage(errorDetail.getMessageCode(), args, locale)
        );
    }
}
