package com.brujula.kerberos.rest.providers.exception;

import com.brujula.kerberos.rest.dto.ErrorMessage;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

@Provider
public class ConstraintValidationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    private Logger logger = LoggerFactory.getLogger(ConstraintValidationExceptionMapper.class);

    private MessageSource messageSource;

    @Autowired
    public ConstraintValidationExceptionMapper( MessageSource messageSource )
    {
        this.messageSource = messageSource;
    }

    @Override
    public Response toResponse(ConstraintViolationException exception) {

        logger.error(ExceptionUtils.getStackTrace(exception));

        List<ErrorMessage> errorMessages = new ArrayList<>();
        exception.getConstraintViolations().stream()
                .forEach(constraintViolation -> errorMessages.add(new ErrorMessage(constraintViolation.getPropertyPath() + ":" +
                        messageSource.getMessage(constraintViolation.getMessage(), null, LocaleContextHolder.getLocale()))));
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(errorMessages)
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}

