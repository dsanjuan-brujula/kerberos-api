package com.brujula.kerberos.rest.providers.exception;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

    private Logger logger = LoggerFactory.getLogger(NotFoundExceptionMapper.class);

    @Override
    public Response toResponse(final NotFoundException exception) {
        logger.error(ExceptionUtils.getStackTrace(exception));
        return Response.status(Response.Status.NOT_FOUND)
                .entity(StringUtils.EMPTY).type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
