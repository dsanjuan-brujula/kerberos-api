package com.brujula.kerberos.rest.providers.exception;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SystemExceptionMapper implements ExceptionMapper<Exception> {

    private Logger logger = LoggerFactory.getLogger(SystemExceptionMapper.class);

    @Override
    public Response toResponse(final Exception exception) {
        logger.error(ExceptionUtils.getStackTrace(exception));
        return Response.serverError().entity(StringUtils.EMPTY).type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
