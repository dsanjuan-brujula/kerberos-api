package com.brujula.kerberos.rest.providers.exception;

import com.brujula.kerberos.rest.exception.UnauthorizedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnauthroizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {

    private Logger logger = LoggerFactory.getLogger(UnauthroizedExceptionMapper.class);

    @Override
    public Response toResponse(UnauthorizedException exception) {
        logger.error(ExceptionUtils.getStackTrace(exception));
        return Response.status(Response.Status.UNAUTHORIZED).entity(StringUtils.EMPTY).type(MediaType.APPLICATION_JSON_TYPE).build();
    }
}
