package com.brujula.kerberos.rest.providers.i18n;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public class ContextualMessageInterpolator extends ResourceBundleMessageInterpolator {

    public ContextualMessageInterpolator() {
        super();
    }

    @Override
    public String interpolate(String template, Context context) {
        return super.interpolate( template, context, LocaleContextHolder.getLocale() );
    }

    @Override
    public String interpolate(String template, Context context, Locale locale) {
        return super.interpolate( template, context, LocaleContextHolder.getLocale() );
    }
}
