package com.brujula.kerberos.rest.providers.i18n;

import org.glassfish.jersey.server.validation.ValidationConfig;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

@Provider
public class ValidationConfigContextResolver implements ContextResolver<ValidationConfig> {

    @Override
    public ValidationConfig getContext(Class<?> aClass) {
        final ValidationConfig config = new ValidationConfig();
        config.messageInterpolator(new ContextualMessageInterpolator());
        return config;
    }

}
