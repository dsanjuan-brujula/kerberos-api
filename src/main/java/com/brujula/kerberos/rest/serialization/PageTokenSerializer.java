package com.brujula.kerberos.rest.serialization;

import com.brujula.kerberos.rest.dto.PageToken;
import com.brujula.kerberos.rest.encoder.Encoder;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class PageTokenSerializer extends JsonSerializer<PageToken> {

    private Encoder encoder;

    public PageTokenSerializer(final Encoder paramsAdapter) {
	this.encoder = paramsAdapter;
    }


    @Override
    public void serialize(PageToken value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
	    gen.writeString(encoder.encode(value));
    }

}
