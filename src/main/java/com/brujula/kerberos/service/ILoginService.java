package com.brujula.kerberos.service;

import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Validated
public interface ILoginService {

    boolean login(final @NotNull @NotEmpty String userName, final @NotNull @NotEmpty String password);

}
