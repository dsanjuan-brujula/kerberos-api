package com.brujula.kerberos.service;

import com.brujula.kerberos.service.entity.IPage;
import com.brujula.kerberos.service.entity.Page;
import com.brujula.kerberos.service.entity.User;
import com.brujula.kerberos.service.jpa.repository.entity.UserEntity;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;


@Validated
public interface IUserService {

    List<User> getAll();

    Page<User> getByPage(IPage page);

    Optional<User> getByUserName(final @NotNull String userName);

    void create(final @Valid User user);

    void updateOrCreate(@Valid User user);

    User update(final @NotNull String userName, final String password, final String fullName);
}
