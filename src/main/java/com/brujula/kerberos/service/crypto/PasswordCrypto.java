package com.brujula.kerberos.service.crypto;

public interface PasswordCrypto {

    String encode(final String password);

    boolean verifyPassword(final String password, final String hash);
}
