package com.brujula.kerberos.service.crypto.impl;

import com.brujula.kerberos.properties.PbkdfCryptoProperties;
import com.brujula.kerberos.service.crypto.PasswordCrypto;
import com.brujula.kerberos.service.crypto.exception.CryptoException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

@Component
public class PBKDF2WithHmacSHA1Crypto implements PasswordCrypto {

    private static final String SPLIT_CHARACTER = ";";

    private PbkdfCryptoProperties properties;
    private final SecureRandom secureRandom;
    private final SecretKeyFactory skf;


    public PBKDF2WithHmacSHA1Crypto(PbkdfCryptoProperties prop) throws NoSuchAlgorithmException {
        this.properties = prop;
        this.skf = SecretKeyFactory.getInstance(prop.getSecretKeyAlgorithm());
        this.secureRandom = SecureRandom.getInstance(prop.getSecureRandomAlgorithm());
    }

    @Override
    public String encode(String password) {
        try {
            return generateStrongPassword(password);
        } catch (InvalidKeySpecException ex) {
            throw new CryptoException("Error encoding password", ex);
        }
    }

    @Override
    public boolean verifyPassword(String password, String hash) {
        try {
            return validatePassword(password, hash);
        } catch (InvalidKeySpecException | DecoderException ex) {
            throw new CryptoException("Error validating password", ex);
        }
    }

    private String generateStrongPassword(final String password) throws InvalidKeySpecException {
        byte[] salt = getSalt();
        Integer iterations = properties.getIterations();
        byte[] hash = generateHash(password, iterations, properties.getKeyLength(), salt);

        StringBuilder strongPassword = new StringBuilder();
        strongPassword.append(iterations).append(SPLIT_CHARACTER);
        strongPassword.append(Hex.encodeHexString(salt)).append(SPLIT_CHARACTER);
        strongPassword.append(Hex.encodeHexString(hash));
        return strongPassword.toString();
    }

    private byte[] generateHash(final String password, final int Iterations, final int keyLength, final byte[] salt) throws InvalidKeySpecException {
        PBEKeySpec pbek = new PBEKeySpec(password.toCharArray(), salt, Iterations, keyLength * Byte.SIZE);
        try {
            return skf.generateSecret(pbek).getEncoded();
        } finally {
            pbek.clearPassword();
        }
    }

    private byte[] getSalt() {
        byte[] salt = new byte[16];
        secureRandom.nextBytes(salt);
        return salt;
    }

    private boolean validatePassword(final String password, String hash) throws InvalidKeySpecException, DecoderException {
        String[] parts = hash.split(SPLIT_CHARACTER);
        String hashHex = parts[2];
        return hashHex.equals(Hex.encodeHexString(generateHash(password,
                Integer.parseInt(parts[0]),
                Hex.decodeHex(hashHex).length,
                Hex.decodeHex(parts[1]))));
    }
}