package com.brujula.kerberos.service.entity;

public interface IPage {

    int getNumber();

    int getTotalPages();

    long getTotalElements();

    int getPageSize();
}
