package com.brujula.kerberos.service.entity;

import java.util.List;

public class Page<T> implements IPage {

    private int number;
    private int totalPages;
    private long totalElements;
    private int pageSize;
    private List<T> content;

    private Page(PageReqBuilder builder) {
        this.number = builder.page;
        this.pageSize = builder.pageSize;
    }
    private Page(PageBuilder<T> builder) {
        this.number = builder.page;
        this.totalPages = builder.totalPages;
        this.totalElements = builder.totalElements;
        this.pageSize = builder.pageSize;
        this.content = builder.content;
    }

    public int getNumber() {
        return number;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public int getPageSize() {
        return pageSize;
    }

    public List<T> getContent() {
        return content;
    }

    public static PageReqBuilder pageReq(int pageSize) {
        return new PageReqBuilder(pageSize);
    }

    public static <K> PageBuilder<K> pageBuilder(int pageSize) {
        return new PageBuilder<>(pageSize);
    }

    public static class PageReqBuilder {
        private int page;
        private int pageSize;

        public PageReqBuilder(final int pageSize) {
            this.pageSize = pageSize;
        }

        public PageReqBuilder withPage(final int page) {
            this.page = page;
            return this;
        }

        public IPage build() {
            return new Page<>(this);
        }
    }

    public static class PageBuilder<K> {
        private int page;
        private int totalPages;
        private long totalElements;
        private int pageSize;
        private List<K> content;

        public PageBuilder(final int pageSize) {
            this.pageSize = pageSize;
        }

        public PageBuilder<K> withPageSize(int pageSize) {
            this.pageSize = pageSize;
            return this;
        }

        public PageBuilder<K> withPage(final int page) {
            this.page = page;
            return this;
        }

        public PageBuilder<K> withTotalPages(final int totalPages) {
            this.totalPages = totalPages;
            return this;
        }

        public PageBuilder<K> withTotalElements(final long totalElements) {
            this.totalElements = totalElements;
            return this;
        }

        public PageBuilder<K> withContent(final List<K> content) {
            this.content = content;
            return this;
        }

        public Page<K> build() {
            return new Page<>(this);
        }
    }
}
