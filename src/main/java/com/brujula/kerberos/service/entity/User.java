package com.brujula.kerberos.service.entity;


import com.brujula.kerberos.Formats;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Validated
public class User {

    private String fullName;

    private String userName;

    private String password;

    private LocalDateTime lastUpdate;

    @NotNull
    @NotEmpty
    public User(final String fullName, final String userName, final String password) {
        setFullName(fullName);
        setUserName(userName);
        setPassword(password);
    }

    public String getFullName() {
        return fullName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setFullName( @Size(min = Formats.FULL_NAME_MIN_SIZE, max = Formats.FULL_NAME_MAX_SIZE )
                             @Pattern(regexp = Formats.FULL_NAME_PATTERN)
                                     String fullName) {
        this.fullName = fullName;
    }

    public void setUserName(@NotNull
                            @NotEmpty
                            @Size(min = Formats.USER_NAME_MIN_SIZE, max = Formats.USER_NAME_MAX_SIZE)
                            @Pattern(regexp = Formats.USER_NAME_PATTERN)
            String userName) {
        this.userName = userName;
    }

    public void setPassword(@Size(min = Formats.PASSWORD_MIN_SIZE)
                            @Pattern(regexp = Formats.PASS_PATTERN)
                                    String password) {
        this.password = password;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
