package com.brujula.kerberos.service.exception;


import java.util.List;

public class ConflictEntityException extends ServiceException {


    public ConflictEntityException(String message) {
        super(message);
    }

    public ConflictEntityException(String messageCode, List<Object> messageParams) {
        super(messageCode, messageParams);
    }
}
