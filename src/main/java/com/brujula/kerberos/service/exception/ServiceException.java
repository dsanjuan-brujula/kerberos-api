package com.brujula.kerberos.service.exception;

import com.brujula.kerberos.rest.dto.ErrorDetail;

import java.util.List;

public class ServiceException extends RuntimeException {

    private final ErrorDetail detail;

    public ServiceException(final ErrorDetail detail, final Throwable cause) {
        super(cause);
        this.detail = detail;
    }

    public ServiceException(final ErrorDetail detail) {
        this.detail = detail;
    }

    public ServiceException(final String message) {
        this.detail = new ErrorDetail.Builder(message).build();
    }

    public ServiceException(final String messageCode, final List<Object> messageParams) {
        this.detail = new ErrorDetail.Builder(messageCode).withParameters(messageParams).build();
    }

    public ErrorDetail getDetail() {
        return detail;
    }
}
