package com.brujula.kerberos.service.exception;


import com.brujula.kerberos.rest.i18n.I18nCodes;

public class UserNotFoundException extends ServiceException {

    public UserNotFoundException() {
        super(I18nCodes.USER_NOT_FOUND);
    }
}
