package com.brujula.kerberos.service.jpa;

import com.brujula.kerberos.service.ILoginService;
import com.brujula.kerberos.service.crypto.PasswordCrypto;
import com.brujula.kerberos.service.jpa.repository.UserRepository;
import com.brujula.kerberos.service.jpa.repository.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class LoginService implements ILoginService {

    private UserRepository userRepository;

    private PasswordCrypto passwordCrypto;

    @Autowired
    public LoginService(final UserRepository userRepository, final PasswordCrypto passwordCrypto) {
        this.userRepository = userRepository;
        this.passwordCrypto = passwordCrypto;
    }

    @Override
    public boolean login(final String userName, final String password) {
        boolean validLogin = false;
        Optional<UserEntity> user = userRepository.findById(userName);
        if (user.isPresent()) {
            validLogin = passwordCrypto.verifyPassword(password, user.get().getPassword());
        }
        return validLogin;
    }
}
