package com.brujula.kerberos.service.jpa;

import com.brujula.kerberos.rest.i18n.I18nCodes;
import com.brujula.kerberos.service.IUserService;
import com.brujula.kerberos.service.entity.IPage;
import com.brujula.kerberos.service.entity.Page;
import com.brujula.kerberos.service.entity.User;
import com.brujula.kerberos.service.exception.ConflictEntityException;
import com.brujula.kerberos.service.exception.UserNotFoundException;
import com.brujula.kerberos.service.jpa.mapper.Mapper;
import com.brujula.kerberos.service.jpa.repository.UserRepository;
import com.brujula.kerberos.service.jpa.repository.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.apache.commons.lang3.Validate.notNull;

@Component
public class UserService implements IUserService {

    private UserRepository repository;

    private Mapper<UserEntity, User> mapper;

    @Autowired
    public UserService(final UserRepository repository, final Mapper<UserEntity, User> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public List<User> getAll() {
        return mapResultList(repository.findAll());
    }

    @Override
    public Page<User> getByPage(IPage page) {
        notNull(page);
        org.springframework.data.domain.Page<UserEntity> result =
                repository.findAll(PageRequest.of(page.getNumber(), page.getPageSize()));

        return Page.<User>pageBuilder(page.getNumber())
                .withTotalElements(result.getTotalElements())
                .withTotalPages(result.getTotalPages())
                .withPageSize(result.getSize())
                .withContent(mapResultList(result.getContent())).build();
    }

    @Override
    public Optional<User> getByUserName(String userName) {
        Optional<UserEntity> user = repository.findById(userName);
        return user.isPresent()? Optional.of(mapper.from(user.get())) : Optional.empty();
    }

    @Override
    public void create(User user) {
        if (this.getByUserName(user.getUserName()).isPresent()) {
            throw new ConflictEntityException(I18nCodes.USER_EXISTS) ;
        }
        repository.save(mapper.to(user));
    }

    @Override
    public void updateOrCreate(User user) {
        repository.save(mapper.to(user));
    }

    @Override
    public User update( String userName, String password, String fullName) {
        User result = null;
        UserEntity user = repository.findById(userName).orElseThrow(UserNotFoundException::new);

        if (password != null || fullName != null) {
            if (password != null) {
                user.setPassword(password);
            }
            if (fullName != null) {
                user.setFullName(fullName);
            }
            user.setLastUpdated( null );
			repository.save(user);
            result = mapper.from( repository.findById( userName ).orElse( user ) );
        }
        return result;
    }

    private List<User> mapResultList(Iterable<UserEntity> users) {
        List<User> result = new ArrayList<>();
        users.forEach(userEntity -> result.add(mapper.from(userEntity)));
        return result;
    }

}
