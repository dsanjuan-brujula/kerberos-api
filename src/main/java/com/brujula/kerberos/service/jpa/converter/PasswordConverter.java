package com.brujula.kerberos.service.jpa.converter;

import com.brujula.kerberos.service.crypto.PasswordCrypto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class PasswordConverter implements AttributeConverter<String, String> {

    private static PasswordCrypto crypto;

    @Autowired
    public PasswordConverter(PasswordCrypto crypto) {
        PasswordConverter.crypto = crypto; //To be able inject spring beans
    }

    @Override
    public String convertToDatabaseColumn(String attribute) {
        return crypto.encode(attribute);
    }

    @Override
    public String convertToEntityAttribute(String dbData) {
        return dbData;
    }
}
