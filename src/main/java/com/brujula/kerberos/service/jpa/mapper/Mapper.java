package com.brujula.kerberos.service.jpa.mapper;

public interface Mapper<O,D> {

    D from(O from);

    O to(D to);

}
