package com.brujula.kerberos.service.jpa.mapper;

import com.brujula.kerberos.service.entity.User;
import com.brujula.kerberos.service.jpa.repository.entity.UserEntity;
import org.springframework.stereotype.Component;

import java.time.ZoneId;

@Component
public class UserMapper implements Mapper<UserEntity, User> {

    @Override
    public User from(final UserEntity from) {
        User user = new User(from.getFullName(), from.getUserName(), from.getPassword());
        if (from.getLastUpdated() != null) {
            user.setLastUpdate(from.getLastUpdated().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        }
        return user;
    }

    @Override
    public UserEntity to(final User to) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(to.getUserName());
        userEntity.setFullName(to.getFullName());
        userEntity.setPassword(to.getPassword());
        if (to.getLastUpdate() != null) {
            userEntity.setLastUpdated(java.util.Date.from(to.getLastUpdate().atZone(ZoneId.systemDefault()).toInstant()));
        }
        return userEntity;
    }
}
