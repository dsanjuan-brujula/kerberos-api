package com.brujula.kerberos.service.jpa.repository;

import com.brujula.kerberos.service.jpa.repository.entity.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, String> {

}
