package com.brujula.kerberos.service.jpa.repository.entity;

import com.brujula.kerberos.Formats;
import com.brujula.kerberos.service.jpa.converter.PasswordConverter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USERS")
public class UserEntity  {

    @Id
    @Column(name = "USER_NAME", length = Formats.USER_NAME_MAX_SIZE)
    private String userName;

    @Column(nullable = false, name = "FULL_NAME", length = Formats.FULL_NAME_MAX_SIZE)
    private String fullName;

    @Column(nullable = false, name = "PASSWORD")
    @Convert(converter = PasswordConverter.class)
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name= "UPDATED_AT", nullable = false, insertable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private Date lastUpdated;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
